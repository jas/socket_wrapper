#include <stdbool.h>

/* simulate socket_wrapper hooks */
#define __FAKE_UID_WRAPPER_SYSCALL_NO 123456789
#define __FAKE_UID_WRAPPER_SYSCALL_RC 987654321
bool uid_wrapper_syscall_valid(long int sysno);
long int uid_wrapper_syscall_va(long int sysno, va_list va);

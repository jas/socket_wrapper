#include "config.h"

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

#ifdef HAVE_SYS_SYSCALL_H
#include <sys/syscall.h>
#endif
#ifdef HAVE_SYSCALL_H
#include <syscall.h>
#endif

#include "swrap_fake_uid_wrapper.h"

/* simulate uid_wrapper hooks */
bool uid_wrapper_syscall_valid(long int sysno)
{
	if (sysno == __FAKE_UID_WRAPPER_SYSCALL_NO) {
		return true;
	}

	return false;
}

long int uid_wrapper_syscall_va(long int sysno, va_list va)
{
	(void) va; /* unused */

	if (sysno == __FAKE_UID_WRAPPER_SYSCALL_NO) {
		errno = 0;
		return __FAKE_UID_WRAPPER_SYSCALL_RC;
	}

	errno = ENOSYS;
	return -1;
}
